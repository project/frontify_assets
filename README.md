# Frontify Assets

This module provides integration between Drupal and the Frontify digital
asset management (DAM) platform. The Frontify Assets Integration for
Drupal simplifies accessing images, videos, and documents from the
central Platform.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/frontify_assets).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/frontify_assets).


## Table of contents
- Features
- Requirements
- Installation
- Configuration
- Maintainers


## Features
1. Image, video, and documents Integration in Drupal from Frontify Assets platform
2. Select Frontify Assets from Frontify in Drupal fields or WYSIWYG editors using the Frontify Media Browser called ‘‘Frontify Finder 2’ version
3. Multiple fields in the same content type
4. Support of Image style for the images
5. Multiple formats for images, videos, and documents
6. Support of color box in views
7. Support of video play inline and popup.


## Requirements

This module requires no modules outside of Drupal core.


## Recommended modules

[Colorbox](https://www.drupal.org/project/markdown)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Generate the Client ID from the Frontify platform
2. Login as Admin to Drupal Application and go to Configuration -> Media -> Frontify Settings
3. Enter Frontify API URL like https://test.frontify.com and Client ID
4. Save the configuration


## Maintainers

- JEET PATEL - [jeetmail72](https://www.drupal.org/u/jeetmail72)
- Shubham Joshi - [Shubh02](https://www.drupal.org/u/shubh02)
