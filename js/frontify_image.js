/**
 * @file
 * Contains the definition of the behaviour jsTestBlackWeight.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';
  /**
   * Attaches the Frontify image JS.
   */
  Drupal.behaviors.FrontifyImage = {
    attach: function () {
      $('input.frontify-image-delete-button').on('click', function (event) {
        event.preventDefault();
        var $field = $(this).parents('.fieldset.fieldset.js-form-item');
        $field.find('.frontify-wrapper-finder-overlay').removeClass('frontify-wrapper-finder-overlay-shown');
        $field.find('img.frontify-image-preview').attr("src", '');
        $field.find('input.frontify-assets-image-url').val('');
        $field.find('input.frontify-assets-image-download').val('');
        $field.find('input.frontify-assets-alt-text').val('');
        $field.find('input.frontify-assets-filesize').val('');
        $field.find('input.frontify-assets-mimetype').val('');
        $field.find('input.frontify-assets-filename').val('');
        $field.removeClass('FrontifyImage');
        $(this).hide();
      });
      $('input.frontify-image-insert-button').on('click', function (event) {
        event.preventDefault();
        var $field = $(this).parents('fieldset.fieldset.js-form-item');
        var allowedExtension = $field.find('.frontify-wrapper-extensions').val();
        var extensionArray = ['png', 'jpg', 'jpeg'];
        if (allowedExtension != '' && allowedExtension != 'undefined') {
          extensionArray = allowedExtension.split(',');
        }
        if (!$field.hasClass('FrontifyImage')) {
          $field.addClass('FrontifyImage');
          var randomString = generateRandomString(5);
          $field.find('.frontify-wrapper-finder-overlay').addClass('frontify-wrapper-finder-overlay-shown');
          $field.find('.frontify-popup-wrapper-button').addClass(randomString)
          var selector = '';
          selector = '.' + randomString;
          if (drupalSettings.FrontifyAssets.client_id == '' || drupalSettings.FrontifyAssets.api_url == '') {
            alert(Drupal.t('Frontify Settings is not configured. Please contact with Administrator.'));
            return false;
          }
          // Add class to mark initialization
          window.FrontifyFinder.create({
            clientId: drupalSettings.FrontifyAssets.client_id,
            domain: drupalSettings.FrontifyAssets.api_url,
            container: selector,
            options: {
              allowMultiSelectfcc: true,
              filters: [
                {
                  key: 'ext',
                  values: extensionArray,
                  inverted: false,
                }
              ],
            }
          })
          .then(finder => {
            finder.onAssetsChosen(assets => {
              if (drupalSettings.FrontifyAssets.debug_mode) {
                console.log(assets);
              }
              
              event.currentTarget.disabled = false;
              $field.find('.frontify-popup-wrapper-button').empty();
              $field.find('.frontify-popup-wrapper-button').hide();
              $field.find('.frontify-wrapper-finder-overlay').removeClass('frontify-wrapper-finder-overlay-shown');
              if (typeof assets[0].downloadUrl === 'undefined' || assets[0].downloadUrl === '') {
                alert(Drupal.t('There is some issue with Frontify Assets. Please re-select the Asset Image'));
                return false;
              }
              $field.find('img.frontify-image-preview').attr("src", assets[0].previewUrl + '?width=100');
              $field.find('input.frontify-assets-image-url').val(assets[0].previewUrl);
              $field.find('input.frontify-assets-image-download').val(assets[0].downloadUrl);
              $field.find('input.frontify-assets-alt-text').val(assets[0].title);
              $field.find('input.frontify-assets-filesize').val(assets[0].size);
              $field.find('input.frontify-assets-mimetype').val(assets[0].extension);
              $field.find('input.frontify-assets-filename').val(assets[0].filename);
              $field.find('input.frontify-image-delete-button').show();
            })
            finder.onCancel(() => {
              event.currentTarget.disabled = false;
              $field.removeClass('FrontifyImage');
              $field.find('.frontify-wrapper-finder-overlay').removeClass('frontify-wrapper-finder-overlay-shown');
              $field.find('.frontify-popup-wrapper-button').empty();
              $field.find('.frontify-popup-wrapper-button').hide();
            })
            finder.mount(document.querySelector(selector));
          })
          .catch(error => {
            event.currentTarget.disabled = false;
            $field.find('.frontify-wrapper-finder-overlay').removeClass('frontify-wrapper-finder-overlay-shown');
            $field.find('.frontify-popup-wrapper-button').empty();
            $field.find('.frontify-popup-wrapper-button').hide();
            console.log(error);
          });
        }
        $field.find(selector).css('display', 'flex');
        return false;
      });
    }

  };
  function generateRandomString(length) {
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var result = '';

    for (var i = 0; i < length; i++) {
      var randomIndex = Math.floor(Math.random() * characters.length);
      result += characters.charAt(randomIndex);
    }

    return result;
  }

})(jQuery, Drupal, drupalSettings);

