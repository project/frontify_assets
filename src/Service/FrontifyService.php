<?php

namespace Drupal\frontify_assets\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Frontify service to manage actions.
 */
class FrontifyService {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The image style entity storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * Constructs a FrontifyService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->imageStyleStorage = $this->entityTypeManager->getStorage('image_style');
  }

  /**
   * Extract URL path & Query string.
   *
   * @param string $uri
   *   Full URL.
   *
   * @return array
   *   Return array of path & query string.
   */
  private function extractUrlQueryString($uri) {
    $queryArray = [];
    $uriArray = parse_url($uri);
    if (!empty($uriArray['path'])) {
      $uri = $uriArray['scheme'] . '://' . $uriArray['host'] . $uriArray['path'];
      // Extract query string into array.
      if (!empty($uriArray['query'])) {
        parse_str($uriArray['query'], $queryArray);
      }
    }
    return [
      'path' => $uri,
      'query' => $queryArray,
    ];
  }

  /**
   * Attach width on frontify image.
   *
   * @param string $uri
   *   Image full URI.
   * @param int|bool $width
   *   Image width.
   * @param bool $preview
   *   Flag to add crop, fp and fp_zoom.
   *
   * @return string
   *   Return image URI with width query string.
   */
  public function attachImageWidth($uri, $width = NULL, $height = NULL, $preview = FALSE) {
    $newUri = $uri;
    $queryArray = [];
    // Attach image style.
    if (!empty($width) || !empty($height)) {
      // Extract Path & query string from URI.
      $urlArray = $this->extractUrlQueryString($uri);
      $queryArray = $urlArray['query'];
      // Add width into Image URL.
      if (!empty($width)) {
        $queryArray['width'] = $width;
      }
      // Add height into Image URL.
      if (!empty($height)) {
        $queryArray['height'] = $height;
      }
      if (!empty($queryArray)) {
        // If it is not preview in Admin.
        if ($preview == FALSE) {
          $queryArray += [
            'crop' => 'fp',
            'fp' => '0.5,0.5',
            'fp_zoom' => 1,
          ];
        }
        $newUri = $urlArray['path'] . '?' . http_build_query($queryArray);
      }
    }
    return $newUri;
  }

  /**
   * Attach image style on the frontify image.
   *
   * @param string $uri
   *   Image full URI.
   * @param string $imageStyle
   *   Image style name.
   *
   * @return string
   *   Return image URI with width query string.
   */
  public function attachImageStyle($uri, $imageStyle = NULL) {
    $newUri = $uri;
    // Extract Path & query string from URI.
    $urlArray = $this->extractUrlQueryString($uri);
    $queryArray = $urlArray['query'];
    // If image style passed.
    if (!empty($imageStyle)) {
      $imageStyle = $this->imageStyleStorage->load($imageStyle);
      if ($imageStyle) {
        // If image style exists, get width & height.
        $effect = $imageStyle->getEffects()->getConfiguration();
        $effect = reset($effect);
        // Add width into Image URL.
        if ($effect['data']['width']) {
          $queryArray['width'] = $effect['data']['width'];
        }
        // Add height into Image URL.
        if ($effect['data']['height']) {
          $queryArray['height'] = $effect['data']['height'];
        }
      }
    }
    // Add query string into image URL.
    if (!empty($queryArray)) {
      $queryArray += [
        'crop' => 'fp',
        'fp' => '0.5,0.5',
        'fp_zoom' => 1,
      ];
      $newUri = $urlArray['path'] . '?' . http_build_query($queryArray);
    }
    return $newUri;
  }

}
