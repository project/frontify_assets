<?php

namespace Drupal\frontify_assets\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\frontify_assets\Service\FrontifyService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Frontify Assets routes.
 */
class FrontifyAssetsController extends ControllerBase {

  /**
   * The frontify_assets.service service.
   *
   * @var \Drupal\frontify_assets\Service\FrontifyService
   */
  protected $service;

  /**
   * The controller constructor.
   *
   * @param \Drupal\frontify_assets\Service\FrontifyService $service
   *   The frontify_assets.service service.
   */
  public function __construct(FrontifyService $service) {
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('frontify_assets.service')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];
    return $build;
  }

}
