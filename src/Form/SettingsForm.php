<?php

namespace Drupal\frontify_assets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Creates authorization form for frontify assets.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'frontify_assets_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'frontify_assets.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('frontify_assets.settings');

    $form['frontify_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Frontify API'),
      '#open' => TRUE,
    ];

    $form['frontify_api']['url'] = [
      '#type' => 'url',
      '#required' => TRUE,
      '#title' => $this->t('URL'),
      '#default_value' => $config->get('frontify_api_url'),
      '#description' => $this->t('Url must be like https://test.frontify.com.')
    ];

    $form['frontify_api']['client_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('frontify_client_id'),
      '#description' => $this->t('Client ID like client-gydatxejthutqthl')
    ];

    $form['debug_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Debug'),
      '#open' => FALSE,
    ];

    $form['debug_settings']['debug_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debug mode'),
      '#description' => $this->t('Enable debug mode and save response logs in DB Log'),
      '#default_value' => $config->get('debug_mode'),
    ];
    $form = parent::buildForm($form, $form_state);
    $form['creds']['actions'] = $form['actions'];
    unset($form['actions']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('frontify_assets.settings');
    $config->set('frontify_api_url', $form_state->getValue('url'));
    $config->set('frontify_client_id', $form_state->getValue('client_id'));
    $config->set('debug_mode', $form_state->getValue('debug_mode'));

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
