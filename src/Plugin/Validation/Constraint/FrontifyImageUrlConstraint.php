<?php

namespace Drupal\frontify_assets\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validation constraint for frontify image URL.
 *
 * @Constraint(
 *   id = "FrontifyImageUrl",
 *   label = @Translation("Link data valid for frontifyImage.", context = "Validation"),
 * )
 */
class FrontifyImageUrlConstraint extends Constraint {

  public $message = "The path '@uri' is invalid.";

}
