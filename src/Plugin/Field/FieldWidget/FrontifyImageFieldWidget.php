<?php

namespace Drupal\frontify_assets\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\frontify_assets\Service\FrontifyService;

/**
 * Frontify image field widget.
 *
 * @FieldWidget(
 *   id = "frontify_image_field_widget",
 *   label = @Translation("Frontify Image Url"),
 *   description = @Translation("A plaintext field for a frontify image URL and alt."),
 *   field_types = {
 *     "frontify_image_field"
 *   }
 * )
 */
class FrontifyImageFieldWidget extends LinkWidget {

  /**
   * The frontify_assets.service service.
   *
   * @var \Drupal\frontify_assets\Service\FrontifyService
   */
  protected $frontifyService;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\frontify_assets\Service\FrontifyService $frontify_service
   *   The Frontify service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, FrontifyService $frontify_service) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->frontifyService = $frontify_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('frontify_assets.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('frontify_assets.settings');
    $item = $items[$delta];
    $field_settings = $this->getFieldSettings();
    // Allowed file extension.
    if (!empty($field_settings['extension'])) {
      $extensions = $field_settings['extension'];
      $element['frontify_wrapper_extensions'] = [
        '#type' => 'textfield',
        '#default_value' => $extensions,
        '#attributes' => [
          'class' => [
            'frontify-wrapper-extensions',
            'hidden',
            'frontify-hidden',
          ],
        ],
      ];
    }

    $element['uri'] = [
      '#type' => 'hidden',
      '#default_value' => $item->uri,
      '#element_validate' => [[get_called_class(), 'validateUriElement']],
      '#maxlength' => 2048,
      '#attributes' => [
        'class' => [
          'frontify-assets-image-url',
          'hidden',
          'frontify-hidden',
        ],
      ],
    ];
    $element['download_uri'] = [
      '#type' => 'hidden',
      '#default_value' => $item->download_uri,
      '#element_validate' => [[get_called_class(), 'validateUriElement']],
      '#maxlength' => 2048,
      '#attributes' => [
        'class' => [
          'frontify-assets-image-download',
          'hidden',
          'frontify-hidden',
        ],
      ],
    ];
    $previewImageUrl = $item->uri;
    // Preview image on "Add more".
    $request = \Drupal::request()->request->all();
    $fieldName = $item->getDataDefinition()->getFieldDefinition()->getName();
    if (!empty($request[$fieldName])) {
      $data = $request[$fieldName];
      $previewImageUrl = $data[$delta]['uri'] ?? NULL;
    }
    // Render image in dimension of 100x100.
    if (!empty($previewImageUrl)) {
      $previewImageUrl = $this->frontifyService->attachImageWidth($previewImageUrl, 100, 100, TRUE);
    }
    $element['frontify_image_preview'] = [
      '#theme' => 'image',
      '#uri' => $previewImageUrl,
      '#alt' => '',
      '#attributes' => [
        'class' => ['frontify-image-preview'],
      ],
    ];
    $element['frontify_wrapper_overlay'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['frontify-wrapper-finder-overlay'],
      ],
    ];
    $element['frontify_wrapper_overlay']['frontify_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['frontify-popup-wrapper-button'],
      ],
    ];

    $element['open'] = [
      '#type' => 'button',
      '#value' => $this->t('Select Frontify Image'),
      '#attributes' => [
        'class' => [
          'frontify-image-insert-button',
          'button--extrasmall',
          'btn-primary',
        ],
      ],
      '#attached' => [
        'library' => [
          'frontify_assets/frontify_image',
        ],
        'drupalSettings' => [
          'FrontifyAssets' =>
          [
            'api_url' => $config->get('frontify_api_url'),
            'client_id' => $config->get('frontify_client_id'),
            'debug_mode' => $config->get('debug_mode'),
          ],
        ],
      ],
      '#limit_validation_errors' => [],
    ];
    $display = 'none';
    if (!empty($item->uri)) {
      $display = '';
    }
    $element['delete'] = [
      '#type' => 'button',
      '#value' => $this->t('Remove'),
      '#attributes' => [
        'class' => [
          'frontify-image-delete-button',
          'button--extrasmall',
          'btn-primary',
        ],
        'style' => ['display:' . $display],
      ],
    ];

    $element['alt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alt text'),
      '#default_value' => isset($item->alt) ? $item->alt : NULL,
      '#maxlength' => 255,
      '#description' => $this->t('Short description of the image used by screen readers and displayed when the image is not loaded. This is important for accessibility.'),
      '#attributes' => [
        'class' => ['frontify-assets-alt-text'],
      ],
    ];

    $element['filesize'] = [
      '#type' => 'hidden',
      '#title' => $this->t('File Size'),
      '#default_value' => $item->filesize ?? $item->filesize,
      '#maxlength' => 255,
      '#attributes' => [
        'class' => ['frontify-assets-filesize'],
      ],
    ];

    $element['mimetype'] = [
      '#type' => 'hidden',
      '#title' => $this->t('File Mime Type'),
      '#default_value' => $item->mimetype ?? $item->mimetype,
      '#maxlength' => 255,
      '#attributes' => [
        'class' => ['frontify-assets-mimetype'],
      ],
    ];

    $element['filename'] = [
      '#type' => 'hidden',
      '#title' => $this->t('File Name'),
      '#default_value' => $item->filename ?? NULL,
      '#maxlength' => 255,
      '#attributes' => [
        'class' => ['frontify-assets-filename'],
      ],
    ];

    $element += [
      '#type' => 'fieldset',
    ];
    return $element;
  }

  /**
   * Form element validation handler for the 'uri' element.
   *
   * Disallows saving inaccessible or untrusted URLs.
   */
  public static function validateUriElement($element, FormStateInterface $form_state, $form) {
    $uri = static::getUserEnteredStringAsUri($element['#value']);
    $form_state->setValueForElement($element, $uri);
    if ($element['#required'] && !$uri) {
      $form_state->setError($element, t('Field @filedname is require!', ['@filedname' => '']));
      return;
    }
  }

}
