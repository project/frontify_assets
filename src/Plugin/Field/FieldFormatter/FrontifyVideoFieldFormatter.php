<?php

namespace Drupal\frontify_assets\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Frontify Video field formatter.
 *
 * @FieldFormatter(
 *   id = "frontify_video_field_formatter",
 *   label = @Translation("Frontify Video Popup"),
 *   description = @Translation("Display the forntify Video thumbnail"),
 *   field_types = {
 *     "frontify_video_field"
 *   }
 * )
 */
class FrontifyVideoFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $values = $item->getValue();
      $element[$delta] = [
        '#theme' => 'frontify_assets_video_formatter',
        '#uri' => $values['uri'] . '?format=mp4',
        '#alt' => $values['alt'],
        '#download_uri' => $values['download_uri'],
      ];
    }

    return $element;
  }

}
