<?php

namespace Drupal\frontify_assets\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Frontify Image field download formatter.
 *
 * @FieldFormatter(
 *   id = "frontify_image_download_formatter",
 *   label = @Translation("Frontify Image Download Url"),
 *   description = @Translation("Frontify Image Download Url"),
 *   field_types = {
 *     "frontify_image_field"
 *   }
 * )
 */
class FrontifyImageDownloadFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $values = $item->getValue();
      $element[$delta] = [
        '#markup' => $values["download_uri"],
      ];
    }

    return $element;
  }

}
