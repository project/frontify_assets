<?php

namespace Drupal\frontify_assets\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;
use Drupal\frontify_assets\Service\FrontifyService;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\colorbox\ElementAttachmentInterface;

/**
 * Frontify Image field formatter.
 *
 * @FieldFormatter(
 *   id = "frontify_colorbox_image",
 *   label = @Translation("Frontify Colorbox Image"),
 *   description = @Translation("Display the forntify image into Colorbox"),
 *   field_types = {
 *     "frontify_image_field"
 *   }
 * )
 */
class FrontifyColorboxImageFormatter extends FormatterBase implements ContainerFactoryPluginInterface {
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The image style entity storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * The frontify_assets.service service.
   *
   * @var \Drupal\frontify_assets\Service\FrontifyService
   */
  protected $frontifyService;

  /**
   * Element attachment allowing library to be attached to pages.
   *
   * @var \Drupal\colorbox\ElementAttachmentInterface
   */
  protected $attachment;

  /**
   * Library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  private $libraryDiscovery;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\frontify_assets\Service\FrontifyService $frontify_service
   *   The Frontify service.
   * @param \Drupal\colorbox\ElementAttachmentInterface $attachment
   *   Allow the library to be attached to the page.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   Library discovery service.
   */
  public function __construct($plugin_id, $plugin_definition,
  FieldDefinitionInterface $field_definition,
  array $settings,
  $label,
  $view_mode,
  array $third_party_settings,
  AccountInterface $current_user,
  EntityStorageInterface $image_style_storage,
  FrontifyService $frontify_service,
  ElementAttachmentInterface $attachment,
  LibraryDiscoveryInterface $libraryDiscovery) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->imageStyleStorage = $image_style_storage;
    $this->frontifyService = $frontify_service;
    $this->attachment = $attachment;
    $this->libraryDiscovery = $libraryDiscovery;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('frontify_assets.service'),
      $container->get('colorbox.attachment'),
      $container->get('library.discovery')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => '',
      'image_style_colorbox' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );
    $element['image_style'] = [
      '#title' => $this->t('Frontify Image style for Content'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ],
    ];

    $element['image_style_colorbox'] = [
      '#title' => $this->t('Frontify Image style for Colorbox'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style_colorbox'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    $image_style_setting = $this->getSetting('image_style');
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = $this->t('Image style: @style', ['@style' => $image_styles[$image_style_setting]]);
    }
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = $this->t('Image style: @style', ['@style' => $image_styles[$image_style_setting]]);
    }
    else {
      $summary[] = $this->t('Original image');
    }
    if (isset($image_styles[$this->getSetting('image_style_colorbox')])) {
      $summary[] = $this->t('Frontify Colorbox image style: @style', ['@style' => $image_styles[$this->getSetting('image_style_colorbox')]]);
    }
    else {
      $summary[] = $this->t('Frontify Colorbox image style: Original image');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $settings = $this->getSettings();
    $settings['colorbox_gallery'] = 'field_post';
    $elements = [];
    $cacheTags = $cacheContext = [];
    $colorboxCacheTags = $colorboxCacheContext = [];
    $height = $width = NULL;
    $colorboxHeight = $colorboxWidth = NULL;
    $entity = $items->getEntity();
    $cacheContext[] = 'url.site';
    // Get content image style.
    $image_style_setting = $this->getSetting('image_style');
    if (!empty($image_style_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_setting);
      if ($image_style) {
        $effect = $image_style->getEffects()->getConfiguration();
        $effect = reset($effect);
        if ($effect['data']['width']) {
          $width = $effect['data']['width'];
        }
        if ($effect['data']['height']) {
          $height = $effect['data']['height'];
        }
        $cacheTags = $image_style->getCacheTags();
        $cacheContext = $image_style->getCacheContexts();
      }
    }
    // Get Colorbox Image style.
    $imageStyleColorboxSettings = $this->getSetting('image_style_colorbox');
    if (!empty($imageStyleColorbox)) {
      $imageStyleColorbox = $this->imageStyleStorage->load($imageStyleColorboxSettings);
      if ($imageStyleColorbox) {
        $effect = $imageStyleColorbox->getEffects()->getConfiguration();
        $effect = reset($effect);
        if ($effect['data']['width']) {
          $colorboxWidth = $effect['data']['width'];
        }
        if ($effect['data']['height']) {
          $colorboxHeight = $effect['data']['height'];
        }
        $colorboxCacheTags = $imageStyleColorbox->getCacheTags();
        $colorboxCacheContext = $imageStyleColorbox->getCacheContexts();
      }
    }
    foreach ($items as $delta => $item) {
      $data_cbox_img_attrs = $variables = [];
      $values = $item->getValue();
      $alt = Xss::filter($values['alt']);
      $galleryId = \Drupal::service('colorbox.gallery_id_generator')->generateId($entity, $item, $settings);
      $variables['attributes']['title'] = $alt;
      $variables['attributes']['data-colorbox-gallery'] = 'colorbox_' . $galleryId;
      $variables['attributes']['class'] = ['colorbox', 'cboxElement'];
      // Do not output an empty 'title' attribute.
      if (!empty($alt)) {
        $variables['image']['#title'] = $alt;
        $data_cbox_img_attrs['title'] = '"title":"' . $alt . '"';
        $data_cbox_img_attrs['alt'] = '"alt":"' . $alt . '"';
      }

      if (!empty($data_cbox_img_attrs)) {
        $variables['attributes']['data-cbox-img-attrs'] = '{' . implode(',', $data_cbox_img_attrs) . '}';
      }
      unset($item->_attributes);
      // Attach image style.
      $values['content_image'] = $this->frontifyService->attachImageWidth($values['uri'], $width, $height);
      $values['colorbox_image'] = $this->frontifyService->attachImageWidth($values['uri'], $colorboxWidth, $colorboxHeight);
      $elements[$delta] = [
        '#theme' => 'frontify_colorbox_formatter',
        '#content_image' => $values['content_image'],
        '#colorbox_image' => $values['colorbox_image'],
        '#alt' => $alt,
        '#attributes' => $variables['attributes'],
        '#cache' => [
          'tags' => Cache::mergeTags($cacheTags, $colorboxCacheTags, $entity->getCacheTags()),
          'contexts' => Cache::mergeContexts($cacheContext, $colorboxCacheContext, $entity->getCacheContexts()),
        ],
      ];
    }

    // Attach the Colorbox JS and CSS.
    if ($this->attachment->isApplicable()) {
      $this->attachment->attach($elements);
      $dompurify = $this->libraryDiscovery->getLibraryByName('colorbox', 'dompurify');
      $dompurify_file = !empty($dompurify['js'][0]['data']) ?
        DRUPAL_ROOT . '/' . $dompurify['js'][0]['data'] : NULL;
      $dompurify_exists = !empty($dompurify) && !empty($dompurify_file) &&
        file_exists($dompurify_file);
      if ($dompurify_exists) {
        $elements['#attached']['library'][] = 'colorbox/dompurify';
      }
    }

    return $elements;
  }

}
