<?php

namespace Drupal\frontify_assets\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Frontify Image field formatter.
 *
 * @FieldFormatter(
 *   id = "frontify_file_url_formatter",
 *   label = @Translation("Frontify File Url Preview"),
 *   description = @Translation("File uploaded preview"),
 *   field_types = {
 *     "frontify_file_field"
 *   }
 * )
 */
class FrontifyFileUrlFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $values = $item->getValue();
      $element[$delta] = [
        '#markup' => $this->t('<a href="@download_url"><img src="@url" alt="@alt" width="auto" height="auto"></a>', [
          '@url' => $values['uri'],
          '@download_url' => $values['download_uri'],
          '@alt' => $values['alt'],
        ]),
      ];
    }

    return $element;
  }

}
