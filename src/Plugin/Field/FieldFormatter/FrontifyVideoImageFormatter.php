<?php

namespace Drupal\frontify_assets\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'Frontify video Image' formatter.
 *
 * @FieldFormatter(
 *   id = "frontify_assets_frontify_video_image",
 *   label = @Translation("Frontify Asset Preview Url"),
 *   field_types = {
 *     "frontify_video_field"
 *   }
 * )
 */
class FrontifyVideoImageFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#markup' => $item->uri . '?format=mp4',
      ];
    }

    return $element;
  }

}
