<?php

namespace Drupal\frontify_assets\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Frontify Image field formatter.
 *
 * @FieldFormatter(
 *   id = "frontify_file_download_formatter",
 *   label = @Translation("Frontify File Download Url"),
 *   description = @Translation("Display the forntify File  with Custom style"),
 *   field_types = {
 *     "frontify_file_field"
 *   }
 * )
 */
class FrontifyFiledDownloadFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $values = $item->getValue();
      $element[$delta] = [
        '#markup' => $values["download_uri"],
      ];
    }

    return $element;
  }

}
