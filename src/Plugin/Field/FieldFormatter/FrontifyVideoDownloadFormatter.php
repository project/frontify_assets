<?php

namespace Drupal\frontify_assets\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Frontify Video field download formatter.
 *
 * @FieldFormatter(
 *   id = "frontify_video_download_formatter",
 *   label = @Translation("Frontify Video Download"),
 *   description = @Translation("Display the Forntify Video Download URL"),
 *   field_types = {
 *     "frontify_video_field"
 *   }
 * )
 */
class FrontifyVideoDownloadFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $values = $item->getValue();
      $element[$delta] = [
        '#markup' => $values['download_uri'],
      ];
    }

    return $element;
  }

}
