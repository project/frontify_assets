<?php

namespace Drupal\frontify_assets\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Frontify File field formatter.
 *
 * @FieldFormatter(
 *   id = "frontify_file_plainurl_formatter",
 *   label = @Translation("Frontify File Plain Download Url"),
 *   description = @Translation("Frontify File Plain Download Url"),
 *   field_types = {
 *     "frontify_file_field"
 *   }
 * )
 */
class FrontifyFilePlainUrlFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $values = $item->getValue();
      $element[$delta] = [
        '#markup' => $values["download_uri"],
      ];
    }

    return $element;
  }

}
