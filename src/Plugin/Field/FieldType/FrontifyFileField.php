<?php

namespace Drupal\frontify_assets\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'frontify_file_field' field type.
 *
 * @FieldType(
 *   id = "frontify_file_field",
 *   label = @Translation("Frontify File Asset"),
 *   description = @Translation("This field is used for frontify file other than image asset integration"),
 *   category = @Translation("General"),
 *   default_widget = "frontify_file_field_widget",
 *   default_formatter = "frontify_file_download_formatter",
 *   constraints = {"FrontifyImageUrl" = {}}
 * )
 */
class FrontifyFileField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['uri'] = DataDefinition::create('uri')->setLabel(t('URI'));
    $properties['download_uri'] = DataDefinition::create('uri')->setLabel(t('Download URI'));
    $properties['alt'] = DataDefinition::create('string')->setLabel(t('File Alt text'));
    $properties['filesize'] = DataDefinition::create('string')->setLabel(t('File size'));
    $properties['mimetype'] = DataDefinition::create('string')->setLabel(t('File mime type'));
    $properties['filename'] = DataDefinition::create('string')->setLabel(t('File name'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    return [
      'columns' => [
        'uri' => [
          'description' => 'The URI of the link.',
          'type' => 'varchar',
          'length' => 2048,
        ],
        'download_uri' => [
          'description' => 'The Downloadable Uri of the link.',
          'type' => 'varchar',
          'length' => 2048,
        ],
        'alt' => [
          'description' => 'Alternative text.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'filesize' => [
          'description' => 'File size.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'mimetype' => [
          'description' => 'Mime Type.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'filename' => [
          'description' => 'File name.',
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
      'indexes' => [
        'uri' => [['uri', 30]],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = [
      'extension' => 'pdf',
    ] + parent::defaultFieldSettings();

    unset($settings['description_field']);
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    // Get base form from FileItem.
    $element = parent::fieldSettingsForm($form, $form_state);
    $element['extension'] = [
      '#type' => 'textfield',
      '#element_validate' => [[static::class, 'validateExtension']],
      '#title' => t('Set the File extension which needs to be allowed to upload'),
      '#default_value' => $this->getSetting('extension'),
    ];

    return $element;
  }

  /**
   * Validate file extension.
   *
   * @param mixed $element
   *   Element object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public static function validateExtension($element, FormStateInterface $form_state) {
    $extensionValue = $element['#value'];
    $pattern = '/^[a-zA-Z0-9]+(?:,[a-zA-Z0-9]+)*$/';
    if (!preg_match($pattern, $extensionValue)) {
      $form_state->setError($element, new TranslatableMarkup('Extension list should be comma separated @name field.', ['@name' => $element['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('uri')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'uri';
  }

  /**
   * {@inheritdoc}
   */
  public function isExternal() {
    return $this->getUrl()->isExternal();
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return Url::fromUri($this->uri);
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadUri() {
    return $this->download_uri;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileSize() {
    return $this->filezie;
  }

  /**
   * {@inheritdoc}
   */
  public function getMimeType() {
    return $this->mimetype;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileName() {
    return $this->filename;
  }

}
